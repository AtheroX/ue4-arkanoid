// Fill out your copyright notice in the Description page of Project Settings.


#include "Paddle.h"
#include "GameFramework/FloatingPawnMovement.h"
#include "Components/StaticMeshComponent.h"

// Sets default values
APaddle::APaddle()
{
	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;


	smPaddle = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("smPaddle"));
	RootComponent = smPaddle;

	smPaddle->SetEnableGravity(false);
	//Lo hace que solo se mueva en ejes 2D
	smPaddle->SetConstraintMode(EDOFMode::XZPlane);
	//Fuerza que use colisiones
	smPaddle->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
	//IMPORTANTE: DEBE TENER ESTE NOMBRE OBLIGATORIAMENTE PARA QUE LO DETECTE BIEN
	smPaddle->SetCollisionProfileName(TEXT("PhyisicsActor"));

	movement = CreateDefaultSubobject<UFloatingPawnMovement>(TEXT("Movement"));

}

// Called when the game starts or when spawned
void APaddle::BeginPlay()
{
	Super::BeginPlay();

}

// Called every frame
void APaddle::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void APaddle::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	//PlayerInputComponent->BindAxis()

}

void APaddle::MoveHorizontal(float axis) {

	AddMovementInput(FVector(axis, 0.f, 0.f), 1.f, false);

}

