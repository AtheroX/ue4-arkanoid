// Fill out your copyright notice in the Description page of Project Settings.


#include "Ball.h"

#include "Components/StaticMeshComponent.h"
#include "GameFramework/ProjectileMovementComponent.h"


// Sets default values
ABall::ABall()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	smBall = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Ball"));
	RootComponent = smBall;

	smBall->SetSimulatePhysics(true);
	smBall->SetEnableGravity(false);
	smBall->SetConstraintMode(EDOFMode::XZPlane);
	smBall->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
	smBall->SetCollisionProfileName(TEXT("PhysicsActor"));

	movement = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("Movement"));
	movement->bShouldBounce = true;
	movement->Bounciness = 1.1f;
	movement->Friction = 0.f;
	movement->Velocity.X = 0.f;
}

// Called when the game starts or when spawned
void ABall::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ABall::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ABall::Launch(){
	if (!started) {
		//El name es donde quieres aplicar el impulso, �til para esqueletos
		//el true es si cambia la velocidad del cuerpo 
		smBall->AddImpulse(FVector(140.f, 0.f, 130.f), FName(), true); 
		started = true;
	}

}

UStaticMeshComponent* ABall::GetBall(){

	return smBall;
}

