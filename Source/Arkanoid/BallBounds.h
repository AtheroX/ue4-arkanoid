// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "BallBounds.generated.h"

class UBoxComponent;
class APaddlePlayerController;

UCLASS()
class ARKANOID_API ABallBounds : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABallBounds();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		UBoxComponent* collision;

	APaddlePlayerController* playerController;

	UFUNCTION()
		void OnHit(UPrimitiveComponent* HitComp, AActor* otherAct,
			UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);


public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
