// Fill out your copyright notice in the Description page of Project Settings.


#include "PaddlePlayerController.h"
#include "Kismet/GameplayStatics.h"
#include "Camera/CameraActor.h"

#include "Ball.h"


APaddlePlayerController::APaddlePlayerController(){


}

void APaddlePlayerController::BeginPlay() {

	TArray<AActor*> CameraActors;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), ACameraActor::StaticClass(), CameraActors);

	FViewTargetTransitionParams params;
	SetViewTarget(CameraActors[0], params);

	myself = Cast<APaddle>(GetPawn());
	SpawnNewBall();
}

void APaddlePlayerController::SetupInputComponent(){

	Super::SetupInputComponent();

	EnableInput(this);

	InputComponent->BindAxis("Horizontal", this, &APaddlePlayerController::MoveHorizontal);
	InputComponent->BindAction("Start", IE_Pressed, this, &APaddlePlayerController::Launch);
}


void APaddlePlayerController::MoveHorizontal(float axis){

	if (myself) { //No es nullptr
		myself->MoveHorizontal(axis);
	}

}

void APaddlePlayerController::Launch(){
	ball->Launch();
}

void APaddlePlayerController::SpawnNewBall(){
	if (!ball)
		ball = nullptr;
	if (ballObj) {
		ball = GetWorld()->SpawnActor<ABall>(ballObj, spawnPos, spawnRot, spawnInfo);
	}
}
