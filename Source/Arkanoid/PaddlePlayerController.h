// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"

#include "Paddle.h"
class ABall;

#include "PaddlePlayerController.generated.h"
UCLASS()
class ARKANOID_API APaddlePlayerController : public APlayerController
{
	GENERATED_BODY()

		APaddlePlayerController();
	
	UFUNCTION()
		virtual void SetupInputComponent() override;

protected:
	virtual void BeginPlay() override;
	void MoveHorizontal(float axis);

	void Launch();

	UPROPERTY(EditAnywhere)
		TSubclassOf<ABall> ballObj;

	ABall* ball;
	FVector spawnPos = FVector(0.f, 0.f, 40.f);
	FRotator spawnRot = FRotator(0.f, 0.f, 0.f);
	FActorSpawnParameters spawnInfo;

public:
	APaddle* myself;
	void SpawnNewBall();

};
