// Fill out your copyright notice in the Description page of Project Settings.


#include "BallBounds.h"
#include "Components/BoxComponent.h"
#include "Kismet/GameplayStatics.h"
#include "PaddlePlayerController.h"

// Sets default values
ABallBounds::ABallBounds()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	collision = CreateDefaultSubobject<UBoxComponent>(TEXT("Collision"));
	
	collision->SetSimulatePhysics(true);
	collision->SetEnableGravity(false);
	collision->SetNotifyRigidBodyCollision(true);
	collision->BodyInstance.SetCollisionProfileName("BlockAllDynamic");

	RootComponent = collision;

}

// Called when the game starts or when spawned
void ABallBounds::BeginPlay()
{
	Super::BeginPlay();

	collision->OnComponentHit.AddDynamic(this, &ABallBounds::OnHit);

	playerController = Cast<APaddlePlayerController>
		(UGameplayStatics::GetPlayerController(GetWorld(), 0));
	
}

// Called every frame
void ABallBounds::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ABallBounds::OnHit(UPrimitiveComponent* HitComp, AActor* otherAct, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit){

	if (otherAct->ActorHasTag("Ball")) {
		otherAct->Destroy();

		playerController->SpawnNewBall();
	}
}
