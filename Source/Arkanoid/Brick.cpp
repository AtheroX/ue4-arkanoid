// Fill out your copyright notice in the Description page of Project Settings.


#include "Brick.h"
#include "Components/StaticMeshComponent.h"
#include "Components/BoxComponent.h"
#include "Ball.h"

// Sets default values
ABrick::ABrick()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	smBrick = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Brick"));

	smBrick->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);

	collision = CreateDefaultSubobject<UBoxComponent>(TEXT("Collision"));

	collision->SetSimulatePhysics(true);
	collision->SetEnableGravity(false);
	collision->SetNotifyRigidBodyCollision(true);
	collision->BodyInstance.SetCollisionProfileName("BlockAllDynamic");
	collision->SetBoxExtent(FVector(25.f, 10.f, 10.f));

	RootComponent = collision;

}

// Called when the game starts or when spawned
void ABrick::BeginPlay()
{
	Super::BeginPlay();

	//collision->OnComponentBeginOverlap.AddDynamic(this, &ABrick::OnOverlapBegin);
	collision->OnComponentHit.AddDynamic(this, &ABrick::OnHit);
}

// Called every frame
void ABrick::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}


void ABrick::OnHit(UPrimitiveComponent* HitComp, AActor* otherAct, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit) {
	UE_LOG(LogTemp, Warning, TEXT("Hit"));
	if (otherAct->ActorHasTag("Ball")) {
		ABall* ball = Cast<ABall>(otherAct);

		FVector ballVel = ball->GetVelocity();
		ballVel *= speedModOnBounce - 1.0f;

		ball->GetBall()->SetPhysicsLinearVelocity(ballVel, true); //A�ade velocidad
		
		FTimerHandle handle;
		GetWorldTimerManager().SetTimer(handle, this, &ABrick::DestroyBrick, 0.05f, false);
	}
}

void ABrick::DestroyBrick() {
	this->Destroy();

}
